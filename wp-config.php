<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'papyrus');

/** MySQL database username */
define('DB_USER', 'papyrus');

/** MySQL database password */
define('DB_PASSWORD', 'pinmuttam');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'X #Y*,Z<f$&Cuy (PlAT0M!9Rf4BUDq,>IvIhC_)Oz<8VxD]ay.nZ]{:M/X>Etk6');
define('SECURE_AUTH_KEY',  'KyT2d#B4F%s6G<%_5YYUNYAILig:s4c+{}h<Of&hMtHW$JP5B2heM;{=[ZKR~)_L');
define('LOGGED_IN_KEY',    'P*ojCWqHD:IR{4=f/MT9R)>]FJ.y,ZKTqVRMF!$;[Kc}1r6A]UfcO3IgdT{qVJ=x');
define('NONCE_KEY',        'Hi:e.ENL,}g3Vt1~BhnXGE+-9FLdu:vE|^Xd)/Fp1.PVvj9f2&$qHT`tAXbb@LYQ');
define('AUTH_SALT',        '/-CFPB|f:DR} _@5zR7voY?HlNMo-jN1?OY=eWcSkqzx3Rs1/kpej@{eGF&]EBdV');
define('SECURE_AUTH_SALT', 'Pa8RJnyi_Jqc7<Eg7lTS{qRl~RUx8 GQCF.OAoA z9g{j+) bVMxw< 8+0TN%Q|J');
define('LOGGED_IN_SALT',   '$x7=KzQ%.P5gY$WW(}faF&rZtq&Hvi?-iD~4XE=oD4V]qx9T&grayb~z&WJPSRom');
define('NONCE_SALT',       'a!GC//>=r3`mweYK7I:crHRCIKv(N&4.2`D_ww).KT(J<^!>vgpmO)/gJM%mf7GT');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
